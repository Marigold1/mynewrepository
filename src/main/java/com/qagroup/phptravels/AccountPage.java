package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class AccountPage {

	private WebDriver driver;

	public AccountPage(WebDriver driver) {
		this.driver = driver;
	}

	@Step("Read current URL")
	@Attachment ("URL")
	public String getCurrentUrl() {
		return this.driver.getCurrentUrl();
	}

	@Step("Read User Name from header")
	@Attachment ("Username")
	public String readUserName() {
		WebElement usernameOnHeader = driver.findElement(
				By.cssSelector(".currency_btn>.sidebar>li:nth-of-type(1)>a.dropdown-toggle.go-text-right"));
		return usernameOnHeader.getText();
	}

}
