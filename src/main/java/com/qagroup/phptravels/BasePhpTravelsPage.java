package com.qagroup.phptravels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class BasePhpTravelsPage {

	@FindBy(css = "nav.navbar-orange.navbar-default")
	private WebElement navigationBar;
	private WebDriver driver;

	public BasePhpTravelsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public NavigationBar navigationBar() {
		return new NavigationBar(navigationBar);
	}

	@Step("Read current URL")
	@Attachment("URL")
	public String getCurrentUrl() {
		return this.driver.getCurrentUrl();
	}

}
