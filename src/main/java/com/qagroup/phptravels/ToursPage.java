package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class ToursPage {

	private WebDriver driver;

	public ToursPage(WebDriver driver) {
		this.driver = driver;

	}

	@Step("Click Tours Page")
	public void testClickToursPage() {
		WebElement navBarTours = driver.findElement(By.xpath(".//li[contains(.,'Tours')]"));
		navBarTours.click();
	}

	@Step("Read current URL")
	@Attachment("URL")
	public String getCurrentUrl() {
		return this.driver.getCurrentUrl();

	}
}
