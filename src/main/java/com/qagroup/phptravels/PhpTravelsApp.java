package com.qagroup.phptravels;

import org.openqa.selenium.WebDriver;

import com.qagroup.tools.Browser;

import io.qameta.allure.Step;

public class PhpTravelsApp {

	private static final String LOGIN_PAGE_URL = "http://www.phptravels.net/login";
	private WebDriver driver;

	public PhpTravelsApp() {
	}

	public MainPage openMainPage() {
		driver = Browser.open();
		driver.get("http://www.phptravels.net");
		return new MainPage(driver);
	}

	@Step("Open Login Page by URL: " + LOGIN_PAGE_URL)
	public LoginPage openLoginPage() {
		driver = Browser.open();
		driver.get("LOGIN_PAGE_URL");
		return new LoginPage(driver);
	}

	@Step("Close application/browser")
	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

}
