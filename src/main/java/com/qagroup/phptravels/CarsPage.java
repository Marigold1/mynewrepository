package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class CarsPage {

	private WebDriver driver;

	public CarsPage(WebDriver driver) {
		this.driver = driver;

	}

	@Step("Click Cars Page")
	public void testClickCarsPage() {
		WebElement navBarCars = driver.findElement(By.xpath("/html/body/nav[1]/div/div/div/ul/li[5]/a"));
		navBarCars.click();
		//WebElementUtils.click(navBarCars);
	}

	@Step("Read current URL")
	@Attachment("URL")
	public  String getCurrentUrl() {
		return this.driver.getCurrentUrl();

		// public static String getCurrentUrl() {
		// TODO Auto-generated method stub
		// return null;
	}

}

// public String getCurrentCarsPagetUrl() {
// return this.driver.getCurrentUrl();
// }
