package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.qagroup.tools.WebElementUtils;

import io.qameta.allure.Step;

public class LoginPage {

	@FindBy(how = How.NAME, using = "username")
	private WebElement usernameField;

	@FindBy(name = "password")
	private WebElement passwordField;

	@FindBy(css = ".loginbtn")
	private WebElement loginButton;

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		// super(driver);
		
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@Step("Login with username <{username}> and password <{password}>")
	public AccountPage loginAs(String username, String password) {

		enterUserName(username);		enterPassword(password);
		clickLoginButton();
		return new AccountPage(driver);
	}


	@Step("Enter username {username}")
	public void enterUserName(String username) {
		WebElement usernameField = driver.findElement(By.name("username"));
		WebElementUtils.sendKeys(usernameField, username);
	}

	@Step("Enter password {password}")
	public void enterPassword(String password) {
		WebElement passwordField = driver.findElement(By.name("password"));
		WebElementUtils.sendKeys(passwordField, password);
	}

	@Step("Click 'Login' button")
	public void clickLoginButton() {
		WebElement loginButton = driver.findElement(By.cssSelector(".loginbtn"));
		WebElementUtils.click(loginButton);
	}
}
