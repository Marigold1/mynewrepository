package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

//import net.phptravels.aut.EnterLocationTest;

public class MainPage extends BasePhpTravelsPage {

	@FindBy(css = ".navbar-static-top #li_myaccount > a")
	private WebElement myAccountButton;

	@FindBy(css = ".navbar-static-top #li_myaccount > a + .dropdown-menu")
	private WebElement myAccountDropdown;

	@FindBy(css = ".select2-result-with-children")
	private WebElement locationDropdown;

	@FindBy(xpath = "/html/body/nav[1]/div/div/div/ul/li[5]/a")
	private WebElement navBarCars;

	@FindBy(xpath = "/html/body/nav[1]/div/div/div/ul/li[3]/a")
	WebElement navBarFlights;

	@FindBy(xpath = "/html/body/nav[1]/div/div/div/ul/li[4]/a")
	WebElement navBarTours;

	private WebDriver driver;

	public MainPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@Step("Navigate To Login Page")
	public LoginPage navigateToLoginPage() {
		// WebElement myAccountButton =
		// driver.findElement(By.cssSelector(".navbar-static-top #li_myaccount > a"));
		myAccountButton.click();

		// WebElement dropdown = driver
		// .findElement(By.cssSelector(".navbar-static-top #li_myaccount > a +
		// .dropdown-menu"));

		WebElement loginOption = myAccountDropdown.findElement(By.xpath(".//a[contains(text(),'Login')]"));
		loginOption.click();

		return new LoginPage(driver);
	}

	public void selectLocation(String city, String country) {
		enterLocation(city);
		waitFor(5);
		selectLocationFromDropdown(city, country);
	}

	public void enterLocation(String city) {
		WebElement spanToClickOn = driver.findElement(By.xpath("//*[contains(text(),'Search by Hotel')]"));
		spanToClickOn.click();

		WebElement locationInput = driver.findElement(By.cssSelector("#select2-drop .select2-search .select2-input"));
		locationInput.sendKeys(city);
	}

	public void selectLocationFromDropdown(String city, String country) {
		String optionXpath = "./ul/li[contains(.,'" + city + "') and contains(.,'" + country + "')]";
		// WebElement optionToSelect =
		// locationDropdown().findElement(By.xpath(optionXpath));
		WebElement optionToSelect = locationDropdown.findElement(By.xpath(optionXpath));
		optionToSelect.click();
	}

	// private WebElement locationDropdown() {
	// return driver.findElement(By.cssSelector(".select2-result-with-children"));
	// }

	public String readSelectedLocation() {
		WebElement locationInput = driver.findElement(By.cssSelector("#HOTELS .select2-choice .select2-chosen"));
		return locationInput.getText();

	}

	@Step("Navigate to Cars Page")
	public CarsPage clickCarsOption() {
		// WebElement navBarCars =
		// driver.findElement(By.xpath("/html/body/nav[1]/div/div/div/ul/li[5]/a"));
		navBarCars.click();
		return new CarsPage(driver);

	}

	@Step("Navigate to Flights Page")
	public FlightsPage clickFlightsOption() {
		navBarFlights.click();
		return new FlightsPage(driver);
	}

	@Step("Navigate to Tours Page")
	public ToursPage clickToursOption() {
		navBarTours.click();
		return new ToursPage(driver);
	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}

	}

}
