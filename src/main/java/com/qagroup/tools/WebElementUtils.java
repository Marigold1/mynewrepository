package com.qagroup.tools;

import org.openqa.selenium.WebElement;

public class WebElementUtils {

	public static void click(WebElement element) {
		highlight(element);
		element.click();
	}
	

	public static void sendKeys(WebElement element, String text) {
		highlight(element);
		element.sendKeys(text);
	}
	
public static String getText(WebElement element) {
		highlight(element);
		return element.getText();
	}
	
	public static void highlight(WebElement element) {
		JavaScriptUtils.highLightElement(element);
	}
}