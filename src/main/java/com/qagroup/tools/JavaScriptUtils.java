package com.qagroup.tools;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.internal.WrapsElement;

public class JavaScriptUtils {
	public static void highLightElement(WebElement element) {

		// WebDriver driver = ((WrapsDriver) element).getWrappedDriver();
		WebDriver driver = unpackWebDriverFromSearchContext(element);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());
		}

		js.executeScript("arguments[0].setAttribute('style','border: dashed 4px red');", element);

	}

	public static WebDriver unpackWebDriverFromSearchContext(SearchContext searchContext) {
		if (searchContext instanceof WebDriver) {
			return (WebDriver) searchContext;
		}

		if (searchContext instanceof WrapsDriver) {
			return unpackWebDriverFromSearchContext(((WrapsDriver) searchContext).getWrappedDriver());
		}

		// Search context it is not only Webdriver. Webelement is search context
		// too.
		// RemoteWebElement and MobileElement implement WrapsDriver
		if (searchContext instanceof WrapsElement) {
			return unpackWebDriverFromSearchContext(((WrapsElement) searchContext).getWrappedElement());
		}

		return null;

	}

}
