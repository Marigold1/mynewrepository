package com.qagroup.tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.qameta.allure.Step;

public class Browser {

	public Browser() {

	}

	@Step("Open Browser")
	public static WebDriver open() {
		WebDriver driver = startBrowser();
		driver.manage().window().maximize();
		return driver;
	}
	
	private static WebDriver startBrowser() {
		String browser = System.getProperty("browser");
		WebDriver driver = null;

		if (browser == null || "chrome".equals(browser.toLowerCase())) {
			driver = startChrome();
		} else if ("firefox".equals(browser.toLowerCase())) {

			driver = startFirefox();
		} else
			throw new RuntimeException("\nUnsupported driver for browser: " + browser + "\n");
		return driver;
	}

	@Step("Google Chrome")
	private static WebDriver startChrome() {
		ChromeDriverManager.getInstance().setup();
		return new ChromeDriver();
	}
	@Step("Firefox")
	private static WebDriver startFirefox() {
		FirefoxDriverManager.getInstance().setup();
		return new FirefoxDriver();
	}
}
