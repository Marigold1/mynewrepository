package com.qagroup.tools;

import org.testng.Assert;

import io.qameta.allure.Step;

public class CustomAsserts {
	
	
	@Step("Actual and Expected should be equal")

	static public void assertEquals(String actual, String expected, String message) {
		Assert.assertEquals(actual, expected, message);
	}

}
