package net.phptravels.location;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;

public class EnterLocationTest {

	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;

	@Test
	public void testEnteringLocation() {
		mainPage = phpTravelsApp.openMainPage();
		waitFor(5);

		mainPage.selectLocation("Lviv", "Ukraine");
		waitFor(5);
		String actualSelectedLocation = mainPage.readSelectedLocation();
		String expectedSelecteLocation = "Lviv, Ukraine";

		Assert.assertEquals(actualSelectedLocation, expectedSelecteLocation, "Incorect selected location");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();

	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
