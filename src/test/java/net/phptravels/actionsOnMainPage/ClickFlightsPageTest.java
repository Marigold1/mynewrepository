package net.phptravels.actionsOnMainPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.CarsPage;
import com.qagroup.phptravels.FlightsPage;
import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.tools.Browser;
//import io.github.bonigarcia.wdm.ChromeDriverManager;
import com.qagroup.tools.CustomAsserts;

public class ClickFlightsPageTest {

	//private WebDriver driver;
	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;
	private FlightsPage fligtsPage;

	@Test

	public void clickFlights() {
		mainPage = phpTravelsApp.openMainPage();
		fligtsPage = mainPage.clickFlightsOption();
		
		String actualUrl = mainPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/flight";
		CustomAsserts.assertEquals(actualUrl, expectedUrl, "Incorrect URL");
		// driver = Browser.open();
		// driver.get("http://www.phptravels.net");
		// WebElement navBarFlights =
		// driver.findElement(By.xpath("/html/body/nav[1]/div/div/div/ul/li[3]/a"));
		// navBarFlights.click();
		waitFor(4);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();

	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
