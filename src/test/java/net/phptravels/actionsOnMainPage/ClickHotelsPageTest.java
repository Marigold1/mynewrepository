package net.phptravels.actionsOnMainPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

//import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.tools.Browser;
//import io.github.bonigarcia.wdm.ChromeDriverManager;

public class ClickHotelsPageTest {
	//private PhpTravelsApp phpTravelsApp  = new PhpTravelsApp();??
	private WebDriver driver;

	@Test
	public void clickHotels() {
		driver = Browser.open();
		driver.get("http://www.phptravels.net");
		
		WebElement navBarHotels = driver.findElement(By.xpath("html/body/nav[1]/div/div/div/ul/li[2]/a"));
		navBarHotels.click();
		waitFor(4);
		}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		if (driver != null);
		{
			driver.quit();
			driver = null;
		}

	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
