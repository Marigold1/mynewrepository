package net.phptravels.actionsOnMainPage;

//import org.openqa.selenium.By;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.testng.Assert;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.AccountPage;
import com.qagroup.phptravels.CarsPage;
//import com.qagroup.phptravels.CarsPage;
import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
//import com.qagroup.tools.Browser;
//import io.github.bonigarcia.wdm.ChromeDriverManager;
import com.qagroup.tools.CustomAsserts;

public class ClickCarsPageTest {
	// private WebDriver driver;
	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;
	private CarsPage carsPage;

	@Test
	public void testClickCarsPage() {
		mainPage = phpTravelsApp.openMainPage();
		carsPage = mainPage.clickCarsOption();

		waitFor(2);

		// return new ClickCarsPageTest (driver);
		String actualUrl = mainPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/cars";

		// String actualUrl = clickCarPageTest.getCurrentUrl();
		// Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");
		CustomAsserts.assertEquals(actualUrl, expectedUrl, "Incorrect URL");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();

	}

	// public void clickCars() {
	// driver = Browser.open();
	// driver.get("http://www.phptravels.net");
	// WebElement navBarCars =
	// driver.findElement(By.xpath("/html/body/nav[1]/div/div/div/ul/li[5]/a"));
	// navBarCars.click();

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
