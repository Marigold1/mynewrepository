package net.phptravels.actionsOnMainPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.CarsPage;
import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.phptravels.ToursPage;
import com.qagroup.tools.Browser;
//import io.github.bonigarcia.wdm.ChromeDriverManager;
import com.qagroup.tools.CustomAsserts;

public class ClickToursPageTest {
	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;
	private ToursPage toursPage;

	@Test
	public void testClickTours() {
		mainPage = phpTravelsApp.openMainPage();
		toursPage = mainPage.clickToursOption();

		String actualUrl = mainPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/tours";
		CustomAsserts.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		waitFor(4);
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();

	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
