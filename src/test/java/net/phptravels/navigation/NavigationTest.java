package net.phptravels.navigation;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.tools.CustomAsserts;

public class NavigationTest {

	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;

	@Test
	public void testNavigationToHotels() {
		mainPage = phpTravelsApp.openMainPage();
		waitFor(2);
		mainPage.navigationBar().selectHotels();

		String actualUrl = mainPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/hotels";

		CustomAsserts.assertEquals(actualUrl, expectedUrl, "URL should be as expected");
	}
	

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

}
