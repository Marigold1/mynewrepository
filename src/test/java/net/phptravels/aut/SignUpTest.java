package net.phptravels.aut;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.qagroup.tools.Browser;
//import io.github.bonigarcia.wdm.ChromeDriverManager;

    public class SignUpTest {
	
	private WebDriver  driver;
	
	@Test
		
	public void testSignUp() {
		driver = Browser.open();
		driver.get("http://www.phptravels.net");
		WebElement myAccount = driver.findElement(By.xpath("html/body/div[3]/div/div/div[2]/ul/ul/li[1]/a"));
				myAccount.click();
				waitFor(4);
		WebElement signUp = driver.findElement(By.xpath("html/body/div[3]/div/div/div[2]/ul/ul/li[1]/ul/li[2]/a"));
		       signUp.click();	
		        waitFor(4);
		WebElement firstNameField  = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[3]/input"));
		firstNameField.sendKeys("Iren");  
		
		WebElement lastNameField  = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[4]/input"));
		lastNameField.sendKeys("IrenL");  
		
		WebElement mobileNumber  = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[5]/input"));
		mobileNumber.sendKeys("380505555555"); 
		
		WebElement emailField  = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[6]/input"));
		emailField.sendKeys("nickname15@i.ua"); 
		
		WebElement password  = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[7]/input"));
		password.sendKeys("123456#"); 
		
		WebElement confirmPassword  = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[8]/input"));
		confirmPassword.sendKeys("123456#"); 
		
		WebElement signUpButton = driver.findElement(By.xpath(".//*[@id='headersignupform']/div[9]/button"));
		signUpButton.click();
		waitFor(4);
		
		String expectedUrl = "http://www.phptravels.net/account/";
		String actualUrl = driver.getCurrentUrl();
		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		WebElement usernameOnHeader = driver.findElement(
				By.cssSelector(".currency_btn>.sidebar>li:nth-of-type(1)>a.dropdown-toggle.go-text-right"));
		String actualUserNameOnHeader = usernameOnHeader.getText();

		Assert.assertEquals(actualUserNameOnHeader, "IREN", "Incorrect username");
	}

	
	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		if (driver != null);
		{
			driver.quit();
			driver = null;
		}

	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	

}
