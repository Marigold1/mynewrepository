package net.phptravels.aut;

//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.AccountPage;
import com.qagroup.phptravels.LoginPage;
import com.qagroup.phptravels.PhpTravelsApp;
//import com.qagroup.tools.Browser;
//import io.github.bonigarcia.wdm.ChromeDriverManager;
//import org.testng.Assert;
import com.qagroup.tools.CustomAsserts;

//import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;

//@Epic("Authentication")
@Feature("Authentication")
@Story("Login")

public class LoginTest  {

	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	// private WebDriver driver;

	private LoginPage loginPage;
	private AccountPage accountPage;

	@Issue("IN-123")
	@TmsLink("TC-456")
	@Link("https://driver.google.com")
	@Test
	public void testLogin() {

		loginPage = phpTravelsApp.openLoginPage();
		accountPage = loginPage.loginAs("user@phptravels.com", "demouser");

		waitFor(4);
		String actualUrl = accountPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/account/";

		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		String actualUserNameOnHeader = accountPage.readUserName();

		CustomAsserts.assertEquals(actualUserNameOnHeader, "JOHNY", "Username on header should be equal to expected.");
	}
		
	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
