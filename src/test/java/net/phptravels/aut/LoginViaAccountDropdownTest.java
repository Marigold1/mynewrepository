package net.phptravels.aut;

//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.AccountPage;
import com.qagroup.phptravels.LoginPage;
import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
//import com.qagroup.tools.Browser;

//import io.github.bonigarcia.wdm.ChromeDriverManager;


public class LoginViaAccountDropdownTest {

	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private MainPage mainPage;
	private LoginPage loginPage;
	private AccountPage accountPage;

	@Test

	public void testViaAccountDropdownTest() {
		mainPage = phpTravelsApp.openMainPage();
		waitFor(2);

		loginPage = mainPage.navigateToLoginPage();
		waitFor(2);

		accountPage = loginPage.loginAs("user@phptravels.com", "demouser");
		waitFor(4);
		String expectedUrl = "http://www.phptravels.net/account/";
		// String actualUrl = driver.getCurrentUrl();
		String actualUrl = accountPage.getCurrentUrl();

		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		String actualUserNameOnHeader = accountPage.readUserName();

		Assert.assertEquals(actualUserNameOnHeader, "JOHNY", "Incorrect username");

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
